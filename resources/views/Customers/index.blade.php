@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><b>Customers</b></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <a href="#" class="btn btn-primary btn-block" data-toggle="modal" data-target="#createModal" type="button">
                            <i class="fas fa-plus mr-2"></i>
                            Create Customer
                        </a>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        @include('alert')
                        <div class="card-body table-responsive p-0" style="height: 300px;">
                            <table class="table table-head-fixed text-nowrap table-striped">
                                <thead>
                                    <tr>
                                        <th class="text-center">Customer Photo</th>
                                        <th class="text-center">First Name</th>
                                        <th class="text-center">Middle Name</th>
                                        <th class="text-center">Last Name</th>
                                        <th class="text-center">Contact</th>
                                        <th class="text-center">Branch</th>
                                        <th class="text-center">Agent</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($customers as $data)
                                        <tr class="text-center">
                                            <td>
                                                @if($data->photo != NULL)
                                                    <img class="profile-user-img img-fluid img-circle" src="<?php echo asset('public/images/') ?>/{{ $data -> photo }}" style="width:50px !important;">
                                                @else
                                                    <img class="profile-user-img img-fluid img-circle" src="<?php echo asset('public/images/default.png') ?>" style="width:50px !important;">
                                                @endif
                                            </td>
                                            <td>{{ $data -> fname }}</td>
                                            <td>{{ $data -> mname }}</td>
                                            <td>{{ $data -> lname }}</td>
                                            <td>{{ $data -> contact }}</td>
                                            <td>{{ $data -> branch }}</td>
                                            <td>{{ $data -> agent }}</td>
                                            <td>
                                                <a href="{{ action('CustomerController@edit',$data->id) }}" class="btn btn-outline-success" type="button">
                                                    Edit
                                                </a>
                                                <a href="{{ action('CustomerController@delete',$data->id) }}" class="btn btn-outline-danger" type="button">
                                                    Delete
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('modals')
    <div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><b>Create Customer</b></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {!! Form::open(['method'=>'POST','action'=>'CustomerController@store','novalidate','files' => 'true']) !!}
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="exampleInputFile">Image</label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="exampleInputFile" name="photo">
                                            <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            {!! Form::label('First Name') !!}
                                            {!! Form::text('fname',null,['class'=>'form-control']) !!}
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            {!! Form::label('Middle Name') !!}
                                            {!! Form::text('mname',null,['class'=>'form-control']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    {!! Form::label('Last Name') !!}
                                    {!! Form::text('lname',null,['class'=>'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('Contact') !!}
                                    {!! Form::text('contact',null,['class'=>'form-control']) !!}
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            {!! Form::label('Branch Assigned') !!}
                                            <select class="form-control" name="branch">
                                                @foreach($branches as $id => $name)
                                                    <option value="{{ $name }}">{{ $name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            {!! Form::label('Agent') !!}
                                            <select class="form-control" name="agent">
                                                @foreach($employees as $id => $name)
                                                    <option value="{{ $name }}">{{ $name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    {!! Form::submit('Save Entry',['class'=>'btn btn-primary']) !!}
                                </div>
                            </div>
                        {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection


