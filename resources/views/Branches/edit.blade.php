@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="text-center"><b>Edit Branches</b></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <a href="{{ route('branch.index')}}" class="btn btn-primary btn-block" type="button">
                        Branch Index
                    </a>
                </ol>
            </div>
        </div>
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-8">
                <div class="col-lg-12 col-xl-12 offset-lg-3 offset-xl-3">
                    <div class="card card-primary">
                        {!! Form::model($branch,['method'=>'PATCH','action'=>['BranchController@update',$branch->id]]) !!}
                            <div class="card-body">
                                <div class="form-group">
                                    {!! Form::label('Branch Name') !!}
                                    {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Enter Branch Name']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('Branch Code') !!}
                                    {!! Form::text('codes',null,['class'=>'form-control','placeholder'=>'Enter Branch Code']) !!}
                                </div>
                            </div>
                            <div class="card-footer">
                                {!! Form::submit('Edit Branch',['class'=>'btn btn-primary']) !!}
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection


