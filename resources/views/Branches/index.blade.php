@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><b>Branches</b></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <a href="#" class="btn btn-primary btn-block" data-toggle="modal" data-target="#createModal" type="button">
                            <i class="fas fa-plus mr-2"></i>
                            Create Branch
                        </a>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        @include('alert')
                        <div class="card-body table-responsive p-0" style="height: 300px;">
                            <table class="table table-head-fixed text-nowrap table-striped">
                                <thead>
                                    <tr>
                                        <th class="text-center">Branch Name</th>
                                        <th class="text-center">Branch Code</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($branches as $data)
                                        <tr class="text-center">
                                            <td>{{ $data -> name }}</td>
                                            <td>{{ $data -> codes }}</td>
                                            <td>
                                                <a href="{{ action('BranchController@edit',$data->id) }}" class="btn btn-outline-success" type="button">
                                                    Edit
                                                </a>
                                                <a href="{{ action('BranchController@delete',$data->id) }}" class="btn btn-outline-danger" type="button">
                                                    Delete
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('modals')
    <div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><b>Create Branch</b></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {!! Form::open(['method'=>'POST','action'=>'BranchController@store']) !!}
                        <div class="card-body">
                            <div class="form-group">
                                {!! Form::label('Branch Name') !!}
                                {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Enter Branch Name']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('Branch Code') !!}
                                {!! Form::text('codes',null,['class'=>'form-control','placeholder'=>'Enter Branch Code']) !!}
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    {!! Form::submit('Save Entry',['class'=>'btn btn-primary']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

