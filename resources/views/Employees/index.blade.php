@extends('layouts.app')
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-8">
                    <h1><b>Employees</b></h1>
                </div>
                <div class="col-sm-2">
                    <ol class="breadcrumb">
                        <a href="#" class="btn btn-primary btn-block" data-toggle="modal" data-target="#createModal" type="button">
                            <i class="fas fa-plus mr-2"></i>
                            Create Employee
                        </a>
                    </ol>
                </div>
                <div class="col-sm-2">
                    <ol class="breadcrumb">
                        <a href="{{ route('employee.show')}}" class="btn btn-outline-primary btn-block" type="button">
                            <i class="fas fa-plus mr-2"></i>
                            Branch Assign
                        </a>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        @include('alert')
                        <div class="card-body table-responsive p-0" style="height: 300px;">
                            <table class="table table-head-fixed text-nowrap table-striped">
                                <thead>
                                    <tr>
                                        <th class="text-center">Employee Photo</th>
                                        <th class="text-center">Employee Name</th>
                                        <th class="text-center">Employee Type</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($employees as $data)
                                        <tr class="text-center">
                                            <td>
                                                @if($data->photo != NULL)
                                                    <img class="profile-user-img img-fluid img-circle" src="<?php echo asset('public/images/') ?>/{{ $data -> photo }}" style="width:50px !important; height:50px !important;">
                                                @else
                                                    <img class="profile-user-img img-fluid img-circle" src="<?php echo asset('public/images/default.png') ?>" style="width:50px !important;">
                                                @endif
                                            </td>
                                            <td>{{ $data -> uname }}</td>
                                            <td>{{ $data -> utype }}</td>
                                            <td>
                                                <a href="{{ action('EmployeeController@edit',$data->id) }}" class="btn btn-outline-success" type="button">
                                                    Edit
                                                </a>
                                                <a href="{{ action('EmployeeController@delete',$data->id) }}" class="btn btn-outline-danger" type="button">
                                                    Delete
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('modals')
    <div class="modal fade" id="createModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><b>Create Employee</b></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <div class="modal-body" style="max-height: 800px">
                    {!! Form::open(['method'=>'POST','action'=>'EmployeeController@store','novalidate','files' => 'true']) !!}
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="exampleInputFile">Photo</label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="exampleInputFile" name="photo">
                                            <label class="custom-file-label" for="exampleInputFile">Choose Photo</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    {!! Form::label('Employee Name') !!}
                                    {!! Form::text('uname',null,['class'=>'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('Employee Type') !!}
                                        <select class="form-control" name="utype">
                                            <option value="Admin">Admin</option>
                                            <option value="Agent">Agent</option>
                                            <option value="Staff">Staff</option>
                                        </select>
                                </div>
                                <div class="form-group">
                                    {!! Form::label('Password') !!}
                                    {!! Form::password('password',['class'=>'form-control password']) !!}
                                </div>
                                <div class="form-group">
                                        {!! Form::label('Branch Assigned') !!}
                                            @foreach($branches as $id => $name)
                                                <label class="form-check">
                                                    <input class="form-check-input" name="branch_id[]" value="{{ $id }}" type="checkbox">
                                                    <label class="form-check-label">{{ $name }}</label>
                                                </label>
                                            @endforeach
                                </div>
                                <div class="form-group">
                                    {!! Form::submit('Save Entry',['class'=>'btn btn-primary']) !!}
                                </div>
                            </div>
                        {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection


