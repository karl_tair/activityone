@extends('layouts.app')

@section('content')
<!-- Main content -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Branch Assigned</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <a href="{{ route('employee.index')}}" class="btn btn-primary btn-block" type="button">
                        Employee Index
                    </a>
                </ol>
            </div>
        </div>
    </div>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-8">
                <div class="col-lg-12 col-xl-12 offset-lg-3 offset-xl-3">
                    <div class="card">
                        @include('alert')
                        <div class="card-body table-responsive p-0" style="height: 500px;">
                            <table class="table table-head-fixed text-nowrap">
                                <thead>
                                    <tr>
                                        <th class="text-center">Employee</th>
                                        <th class="text-center">Branch</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($employees as $data)
                                        <tr class="text-center">
                                            <td>{{ @$data -> employee -> uname }}</td>
                                            <td>{{ @$data -> branch -> name }}</td>
                                            <td>
                                                <a href="{{ action('EmployeeController@delete_assigned',$data->id) }}" class="btn btn-outline-danger" type="button">
                                                    Delete
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection


