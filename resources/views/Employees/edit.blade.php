@extends('layouts.app')
@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="text-center"><b>Edit Employee</b></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <a href="{{ route('employee.index')}}" class="btn btn-primary btn-block" type="button">
                        Employee Index
                    </a>
                </ol>
            </div>
        </div>
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-8">
                <div class="col-lg-12 col-xl-12 offset-lg-3 offset-xl-3">
                    <div class="card card-primary">
                        {!! Form::model($employee,['method'=>'PATCH','action'=>['EmployeeController@update',$employee->id]]) !!}
                            <div class="card-body">
                                <div class="form-group">
                                    {!! Form::label('Employee Name') !!}
                                    {!! Form::text('uname',null,['class'=>'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('Employee Type') !!}
                                        <select class="form-control" name="utype">
                                            <option value="Admin">Admin</option>
                                            <option value="Agent">Agent</option>
                                            <option value="Staff">Staff</option>
                                        </select>
                                </div>
                            </div>
                            <div class="card-footer"> 
                                {!! Form::submit('Edit Employee',['class'=>'btn btn-primary']) !!}
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection


