@extends('layouts.app')
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><b>Products</b></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <a href="#" class="btn btn-primary btn-block" data-toggle="modal" data-target="#createModal" type="button">
                            <i class="fas fa-plus mr-2"></i>
                            Add Product
                        </a>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        @include('alert')
                        <div class="card-body table-responsive p-0" style="height: 300px;">
                            <table class="table table-head-fixed text-nowrap table-striped">
                                <thead>
                                    <tr>
                                        <th class="text-center">Product Name</th>
                                        <th class="text-center">Product Code</th>
                                        <th class="text-center">Arrival Date</th>
                                        <th class="text-center">Expiry</th>
                                        <th class="text-center">Selling Price</th>
                                        <th class="text-center">Original Price</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($product as $data)
                                        <tr class="text-center">
                                            <td>{{ $data -> name }}</td>
                                            <td>{{ $data -> code }}</td>
                                            <td>{{ $data -> date_arrival }}</td>
                                            <td>{{ $data -> expiry }}</td>
                                            <td>{{ $data -> sprice }}</td>
                                            <td>{{ $data -> oprice }}</td>
                                            <td>
                                                <a href="{{ action('ProductController@edit',$data->id) }}" class="btn btn-outline-success" type="button">
                                                    Edit
                                                </a>
                                                <a href="{{ action('ProductController@delete',$data->id) }}" class="btn btn-outline-danger" type="button">
                                                    Delete
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('modals')
    <div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><b>Add Product</b></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {!! Form::open(['method'=>'POST','action'=>'ProductController@store','novalidate','files' => 'true']) !!}
                            <div class="card-body">
                                <div class="form-group">
                                    {!! Form::label('Product Code') !!}
                                    {!! Form::text('code',null,['class'=>'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    <div class="form-group">
                                        {!! Form::label('Product Name') !!}
                                        {!! Form::text('name',null,['class'=>'form-control']) !!}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            {!! Form::label('Arrival Date') !!}
                                            {!! Form::text('date_arrival',null,['class'=>'form-control']) !!}
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            {!! Form::label('Expiration Date') !!}
                                            {!! Form::text('expiry',null,['class'=>'form-control']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            {!! Form::label('Selling Price') !!}
                                            {!! Form::text('sprice',null,['class'=>'form-control']) !!}
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            {!! Form::label('Original Price') !!}
                                            {!! Form::text('oprice',null,['class'=>'form-control']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    {!! Form::submit('Save Entry',['class'=>'btn btn-primary']) !!}
                                </div>
                            </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection