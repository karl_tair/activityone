@extends('layouts.app')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="text-center"><b>Edit Product</b></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <a href="{{ route('product.index')}}" class="btn btn-primary btn-block" type="button">
                        Product Index
                    </a>
                </ol>
            </div>
        </div>
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-8">
                <div class="col-lg-12 col-xl-12 offset-lg-3 offset-xl-3">
                    <div class="card card-primary">
                        {!! Form::model($product,['method'=>'PATCH','action'=>['ProductController@update',$product->id]]) !!}
                            <div class="card-body">
                                <div class="form-group">
                                    {!! Form::label('Product Code') !!}
                                    {!! Form::text('code',null,['class'=>'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    <div class="form-group">
                                        {!! Form::label('Product Name') !!}
                                        {!! Form::text('pname',null,['class'=>'form-control']) !!}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            {!! Form::label('Arrival Date') !!}
                                            {!! Form::text('date_arrival',null,['class'=>'form-control']) !!}
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            {!! Form::label('Expiration Date') !!}
                                            {!! Form::text('expiry',null,['class'=>'form-control']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            {!! Form::label('Selling Price') !!}
                                            {!! Form::text('sprice',null,['class'=>'form-control']) !!}
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            {!! Form::label('Original Price') !!}
                                            {!! Form::text('oprice',null,['class'=>'form-control']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    {!! Form::submit('Save Entry',['class'=>'btn btn-primary']) !!}
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection


