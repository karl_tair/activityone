@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <h2 class="text-center display-4"><b>Reports</b></h2>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-6">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="text-center"><b>Sales</b></h4>
                        </div>
                        <div class="card-footer d-flex justify-content-center">
                            <a href="{{ route('sale.export') }}" class="btn btn-outline-success">
                                <i class="fas fa-download mr-2"></i>Download
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="text-center"><b>Products</b></h4>
                        </div>
                        <div class="card-footer d-flex justify-content-center">
                            <a href="{{ route('product.export') }}" class="btn btn-outline-success">
                                <i class="fas fa-download mr-2"></i>Download
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
