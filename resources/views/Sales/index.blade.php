@extends('layouts.app')
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><b>Sales</b></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <a href="#" class="btn btn-primary btn-block" data-toggle="modal" data-target="#createModal" type="button">
                            <i class="fas fa-plus mr-2"></i>
                            Create Sale
                        </a>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        @include('alert')
                        <div class="card-body table-responsive p-0" style="height: 300px;">
                            <table class="table table-head-fixed text-nowrap table-striped">
                                <thead>
                                    <tr>
                                        <th class="text-center">Product Name</th>
                                        <th class="text-center">Price</th>
                                        <th class="text-center">Quantity</th>
                                        <th class="text-center">Total</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($sale as $data)
                                        <tr class="text-center">
                                            <td>{{ $data -> name }}</td>
                                            <td>{{ $data -> price }}</td>
                                            <td>{{ $data -> quantity }}</td>
                                            <td>{{ $data -> total }}</td>
                                            <td>
                                                <a href="{{ action('SaleController@delete',$data->id) }}" class="btn btn-outline-danger" type="button">
                                                    Delete
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('modals')
    <div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><b>Create Sale</b></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {!! Form::open(['method'=>'POST','action'=>'SaleController@store','novalidate','files' => 'true']) !!}
                            <div class="card-body">
                                <div class="form-group">
                                    {!! Form::label('Product Name') !!}
                                    <select class="form-control" name="name">
                                        @foreach($product as $id => $name)
                                            <option value="{{ $name }}">{{ $name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    {!! Form::label('Price') !!}
                                    <select class="form-control" name="price">
                                        @foreach($products as $data)
                                            <option value="{{ $data -> sprice }}">{{ $data -> name }} = {{ $data -> sprice }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group"> 
                                    {!! Form::label('Quantity') !!}
                                    {!! Form::text('quantity',null,['class'=>'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::submit('Save Sale',['class'=>'btn btn-primary']) !!}
                                </div>
                            </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection