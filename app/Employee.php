<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $guarded = [];

    public function branch()
    {
        return $this->belongsTo('App\Branch', 'branch_id', 'id');
    }

    public function branch_employees()
    {
        return $this->hasMany('App\Branch_Employee', 'branch_id', 'id');
    }
}
