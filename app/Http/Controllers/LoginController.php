<?php

namespace App\Http\Controllers;

use Request;
use Auth;
use Validator;
use Alert;

class LoginController extends Controller
{

    public function index()
    {
        return view('login');
    }

    public function store()
    {
        $validator = Validator::make(
            Request::all(),
            [
                'name'                          =>          'required',
                'password'                      =>          'required',
            ],
            [
                'name.required'              =>          'Username required',
                'password.required'          =>          'Password required',
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        if (Auth::attempt(['name' => Request::input('name'), 'password' => Request::input('password')])) {
            return redirect()->route('branch.index');
        } else {
            return redirect()->back();
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }
}
