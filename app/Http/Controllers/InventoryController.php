<?php

namespace App\Http\Controllers;

use App\Inventory;
use App\Product;
use Request;
use Validator;

class InventoryController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $inventories = Inventory::get();
        $product = Product::pluck('name', 'id');
        return view('inventories.index', compact('inventories', 'product'));
    }

    public function store()
    {
        $validator = Validator::make(
            Request::all(),
            [
                'pname'                             =>          'required|unique:inventories',
                'quantity'                          =>          'required',
                'stocks'                            =>          'required',
            ],
            [
                'pname.required'                 =>          'Product Name Required',
                'quantity.required'              =>          'Quantity Required',
                'stocks.required'                =>          'Stocks Required',
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        Inventory::create(Request::all());
        return redirect()->route('inventory.index');
    }

    public function edit($id)
    {
        $inventory = Inventory::find($id);
        return view('inventories.edit', compact('inventory'));
    }

    public function update($id)
    {
        $inventory = Inventory::find($id);
        $validator = Validator::make(
            Request::all(),
            [
                'pname'                 =>          "required|unique:inventories,pname,$inventory->id,id",
                'quantity'              =>          "required",
                'stocks'                =>          "required",
            ],
            [
                'pname.required'         =>          'Product Name Required',
                'quantity.required'      =>          'Quantity Required',
                'stocks.required'        =>          'Stocks Required',
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $inventory->update(Request::all());
        return redirect()->route('inventory.index');
    }

    public function delete($id)
    {
        $inventory = Inventory::find($id);
        $inventory->delete();
        return redirect()->route('inventory.index');
    }
}
