<?php

namespace App\Http\Controllers;

use App\Branch;
use App\User;
use App\Employee;
use App\Branch_Employee;
use Request;
use Validator;
use Illuminate\Support\Str;

class EmployeeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $employees = Employee::with('branch')->get();
        $branches = Branch::pluck('name', 'id');
        return view('employees.index', compact('employees', 'branches'));
    }

    public function show()
    {
        $employees = Branch_Employee::with('branch')->get();
        return view('employees.show', compact('employees'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make(
            Request::all(),
            [
                'uname'                          =>          'required',
                'utype'                          =>          'required',
                'branch_id'                      =>          'required',
            ],
            [
                'uname.required'                 =>          'Employee Name Required',
                'utype.required'                 =>          'Employee Type Required',
                'branch_id.required'             =>          'Please Assign Branch',
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $file = Request::file('photo');
        if ($file !== NULL) {
            $extension = $file->getClientOriginalExtension();
            $filename = Str::random(50) . '.' . $extension;
            $file->move(public_path() . '/images', $filename);
        } else {
            $filename = 'default.png';
        }

        $last_id = Employee::create([
            'uname'              =>      Request::get('uname'),
            'utype'              =>      Request::get('utype'),
            'password'           =>      \Hash::make(preg_replace('/\s+/', '', Request::get('password'))),
            'photo'              =>      $filename,
        ])->id;

        foreach (Request::get('branch_id') as $key => $value) {
            Branch_Employee::create([
                'branch_id'             =>          $value,
                'employee_id'           =>          $last_id,
            ]);
        }

        User::create([
            'name'               =>      Request::get('uname'),
            'utype'              =>      Request::get('utype'),
            'password'           =>      \Hash::make(preg_replace('/\s+/', '', Request::get('password'))),
            'photo'              =>      $filename,
        ]);
        return redirect()->route('employee.index');
    }

    public function edit($id)
    {
        $employee = Employee::find($id);
        $branches = Branch::pluck('name', 'id');
        return view('employees.edit', compact('employee', 'branches'));
    }

    public function update($id)
    {
        $employee = Employee::find($id);
        $validator = Validator::make(
            Request::all(),
            [
                'uname'                 =>          "required",
                'utype'                 =>          "required",
            ],
            [
                'uname.required'        =>          'Employee Name Required',
                'utype.required'        =>          'Employee Type Required',
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $employee->update(Request::all());
        return redirect()->route('employee.index');
    }

    public function delete($id)
    {
        $employee = Employee::find($id);
        $employee->delete();
        return redirect()->route('employee.index');
    }

    public function delete_assigned($id)
    {
        $employee = Branch_Employee::find($id);
        $employee->delete();
        return redirect()->route('employee.show');
    }
}
