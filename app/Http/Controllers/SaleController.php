<?php

namespace App\Http\Controllers;

use App\Sale;
use App\Inventory;
use App\Product;
use Request;
use Validator;

class SaleController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $sale = Sale::get();
        $products = Product::get();
        $product = Product::pluck('name', 'id');
        return view('sales.index', compact('sale', 'products', 'product'));
    }

    public function store()
    {
        $validator = Validator::make(
            Request::all(),
            [
                'name'                           =>          'required',
                'quantity'                       =>          'required',
            ],
            [
                'name.required'                  =>          'Product Name Required',
                'quantity.required'              =>          'Quantity Required',

            ]
        );

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        Sale::create([
            'name'               =>      Request::get('name'),
            'quantity'           =>      Request::get('quantity'),
            'price'              =>      Request::get('price'),
            'total'              =>      Request::get('price') * Request::get('quantity'),
        ]);
        return redirect()->route('sale.index');
    }

    public function delete($id)
    {
        $sale = Sale::find($id);
        $sale->delete();
        return redirect()->route('sale.index');
    }
}
