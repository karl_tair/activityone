<?php

namespace App\Http\Controllers;

use App\Product;
use App\Inventory;
use Request;
use Validator;

class ProductController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $product = Product::get();
        $inventories = Inventory::pluck('pname', 'id');
        return view('products.index', compact('product', 'inventories'));
    }

    public function store()
    {
        $validator = Validator::make(
            Request::all(),
            [
                'name'                           =>          'required|unique:products',
                'code'                           =>          'required|unique:products',
                'date_arrival'                   =>          'required',
                'expiry'                         =>          'required',
                'sprice'                         =>          'required',
                'oprice'                         =>          'required',
            ],
            [
                'name.required'                  =>          'Product Name Required',
                'code.required'                  =>          'Product Code Required',
                'date_arrival.required'          =>          'Arrival Date Required',
                'expiry.required'                =>          'Expiration Date Required',
                'sprice.required'                =>          'Selling Price Required',
                'oprice.required'                =>          'Original Price Required',

            ]
        );

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        Product::create(Request::all());
        return redirect()->route('product.index');
    }

    public function edit($id)
    {
        $product = Product::find($id);
        return view('products.edit', compact('product'));
    }

    public function update($id)
    {
        $product = Product::find($id);
        $validator = Validator::make(
            Request::all(),
            [
                'name'                           =>          "required|unique:products,name,$product->id,id",
                'code'                           =>          "required|unique:products,code,$product->id,id",
                'date_arrival'                   =>          "required",
                'expiry'                         =>          "required",
                'sprice'                         =>          "required",
                'oprice'                         =>          "required",
            ],
            [
                'name.required'                  =>          'Product Name Required',
                'code.required'                  =>          'Product Code Required',
                'date_arrival.required'          =>          'Arrival Date Required',
                'expiry.required'                =>          'Expiration Date Required',
                'sprice.required'                =>          'Selling Price Required',
                'oprice.required'                =>          'Original Price Required',
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $product->update(Request::all());
        return redirect()->route('product.index');
    }

    public function delete($id)
    {
        $product = Product::find($id);
        $product->delete();
        return redirect()->route('product.index');
    }
}
