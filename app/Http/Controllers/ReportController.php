<?php

namespace App\Http\Controllers;

use App\Report;
use Request;
use Validator;

class ReportController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('reports.index');
    }
}
