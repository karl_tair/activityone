<?php

namespace App\Http\Controllers;

use App\Branch;
use Request;
use Validator;

class BranchController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $branches = Branch::get();
        return view('branches.index', compact('branches'));
    }

    public function store()
    {
        $validator = Validator::make(
            Request::all(),
            [
                'name'                           =>          'required|unique:branches',
                'codes'                          =>          'required|unique:branches',
            ],
            [
                'name.required'                  =>          'Branch Name Required',
                'codes.required'                 =>          'Branch Code Required',
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        Branch::create(Request::all());
        return redirect()->route('branch.index');
    }

    public function edit($id)
    {
        $branch = Branch::find($id);
        return view('branches.edit', compact('branch'));
    }

    public function update($id)
    {
        $branch = Branch::find($id);
        $validator = Validator::make(
            Request::all(),
            [
                'name'                  =>          "required|unique:branches,name,$branch->id,id",
                'codes'                 =>          "required|unique:branches,codes,$branch->id,id",
            ],
            [
                'name.required'         =>          'Branch Name Required',
                'codes.required'        =>          'Branch Code Required',
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $branch->update(Request::all());
        return redirect()->route('branch.index');
    }

    public function delete($id)
    {
        $branch = Branch::find($id);
        $branch->delete();
        return redirect()->route('branch.index');
    }
}
