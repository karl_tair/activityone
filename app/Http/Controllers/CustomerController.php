<?php

namespace App\Http\Controllers;

use App\Branch;
use App\Employee;
use App\Customer;
use Request;
use Validator;
use Illuminate\Support\Str;

class CustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $customers = Customer::with('branch', 'employee')->get();
        $employees = Employee::pluck('uname', 'id');
        $branches  = Branch::pluck('name', 'id');
        return view('customers.index', compact('employees', 'branches', 'customers'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make(
            Request::all(),
            [
                'fname'                          =>          'required',
                'lname'                          =>          'required',
                'contact'                        =>          'required|string|min:8|max:11',
                'branch'                         =>          'required',
                'agent'                          =>          'required',
            ],
            [
                'fname.required'                          =>          'First Name Required',
                'lname.required'                          =>          'Last Name Required',
                'contact.required'                        =>          'Contact Required',
                'branch.required'                         =>          'Branch Assignation Required',
                'agent.required'                          =>          'Agent Assignation Required',
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }


        $file = Request::file('photo');
        if ($file !== NULL) {
            $extension = $file->getClientOriginalExtension();
            $filename = Str::random(50) . '.' . $extension;
            $file->move(public_path() . '/images', $filename);
        } else {
            $filename = 'default.png';
        }

        Customer::create([
            'fname'              =>      Request::get('fname'),
            'mname'              =>      Request::get('mname'),
            'lname'              =>      Request::get('lname'),
            'contact'            =>      Request::get('contact'),
            'branch'             =>      Request::get('branch'),
            'agent'              =>      Request::get('agent'),
            'photo'              =>      $filename,
        ]);
        return redirect()->route('customer.index');
    }


    public function edit($id)
    {
        $customers = Customer::find($id);
        $employees = Employee::pluck('uname', 'id');
        $branches  = Branch::pluck('name', 'id');
        return view('customers.edit', compact('employees', 'branches', 'customers'));
    }

    public function update($id)
    {
        $customers = Customer::find($id);
        $validator = Validator::make(
            Request::all(),
            [
                'fname'                          =>          'required',
                'lname'                          =>          'required',
                'contact'                        =>          'required|string|min:8|max:11',
                'branch'                         =>          'required',
                'agent'                          =>          'required',
            ],
            [
                'fname.required'                          =>          'First Name Required',
                'lname.required'                          =>          'Last Name Required',
                'contact.required'                        =>          'Contact Required',
                'branch.required'                         =>          'Branch Assignation Required',
                'agent.required'                          =>          'Agent Assignation Required',
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $customers->update(Request::all());
        return redirect()->route('customer.index');
    }

    public function delete($id)
    {
        $customer = Customer::find($id);
        $customer->delete();
        return redirect()->route('customer.index');
    }
}
