<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guarded = [];

    public function Inventory()
    {
        return $this->belongsTo('App\Inventory', 'product_id', 'id');
    }

    public function Product()
    {
        return $this->belongsTo('App\Product', 'product_id', 'id');
    }
}
