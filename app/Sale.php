<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    protected $guarded = [];

    public function Inventory()
    {
        return $this->belongsTo('App\Inventory', 'product_id', 'id');
    }

    public function Product()
    {
        return $this->belongsTo('App\Product', 'name', 'id');
    }
}
