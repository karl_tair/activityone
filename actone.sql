-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.33 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for actone
CREATE DATABASE IF NOT EXISTS `actone` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `actone`;

-- Dumping structure for table actone.branches
CREATE TABLE IF NOT EXISTS `branches` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `codes` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table actone.branches: ~4 rows (approximately)
/*!40000 ALTER TABLE `branches` DISABLE KEYS */;
INSERT INTO `branches` (`id`, `name`, `codes`, `created_at`, `updated_at`) VALUES
	(8, 'Davao', 'DVO', '2022-03-11 09:39:00', '2022-03-11 09:39:00'),
	(9, 'Ilo-Ilo', 'ILO', '2022-03-11 09:39:08', '2022-03-11 09:39:08'),
	(10, 'Puerto Princesa', 'PPS', '2022-03-12 06:52:16', '2022-03-12 06:52:16'),
	(11, 'Bacolod', 'BCD', '2022-03-12 06:52:27', '2022-03-12 06:52:27'),
	(12, 'Tacloban', 'TAC', '2022-03-12 06:52:37', '2022-03-12 06:52:37');
/*!40000 ALTER TABLE `branches` ENABLE KEYS */;

-- Dumping structure for table actone.branch__employees
CREATE TABLE IF NOT EXISTS `branch__employees` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) NOT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table actone.branch__employees: ~2 rows (approximately)
/*!40000 ALTER TABLE `branch__employees` DISABLE KEYS */;
INSERT INTO `branch__employees` (`id`, `branch_id`, `employee_id`, `created_at`, `updated_at`) VALUES
	(22, 8, 39, '2022-03-11 09:47:11', '2022-03-11 09:47:11'),
	(23, 9, 39, '2022-03-11 09:47:11', '2022-03-11 09:47:11'),
	(24, 10, 40, '2022-03-12 06:54:19', '2022-03-12 06:54:19');
/*!40000 ALTER TABLE `branch__employees` ENABLE KEYS */;

-- Dumping structure for table actone.customers
CREATE TABLE IF NOT EXISTS `customers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `fname` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mname` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lname` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `branch` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `agent` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '12345',
  `photo` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table actone.customers: ~2 rows (approximately)
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` (`id`, `fname`, `mname`, `lname`, `contact`, `branch`, `agent`, `password`, `photo`, `created_at`, `updated_at`) VALUES
	(1, 'Customer', 'One', 'Entry', '09090909090', 'Bacolod', 'Employee_Two', '12345', 'tzUAcCLRK1NF3e0fL4CTtJwGsH8G22dQKERat6zQZR4czjlz8W.jpg', '2022-03-12 06:55:46', '2022-03-12 06:55:46'),
	(2, 'Karl Francon', 'Galila', 'Tair', '09090909090', 'Puerto Princesa', 'Employee_Two', '12345', 'default.png', '2022-03-12 06:56:21', '2022-03-12 06:56:21');
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;

-- Dumping structure for table actone.employees
CREATE TABLE IF NOT EXISTS `employees` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uname` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `utype` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table actone.employees: ~1 rows (approximately)
/*!40000 ALTER TABLE `employees` DISABLE KEYS */;
INSERT INTO `employees` (`id`, `uname`, `utype`, `password`, `photo`, `created_at`, `updated_at`) VALUES
	(30, 'Kiki', 'Agent', '$2y$10$v0PrRYQAQUJEEs5gsnhgpu7O0SHZk6RC8TvCRclNYcbyeVhgYRU06', 'default.png', '2022-03-04 09:10:10', '2022-03-04 09:10:10'),
	(39, 'Employee_One', 'Agent', '$2y$10$F2eebyz9.b8rtc5RkERVp.96NREH3MCVOHs8C4U8wjJY3udcajLnC', '9efNkN91oFfsE2WsZ7yHTXzpW4KmrpTkDJdaZzONEXBodo5NSE.jpg', '2022-03-11 09:47:11', '2022-03-11 09:47:11'),
	(40, 'Employee_Two', 'Staff', '$2y$10$9mBBxYxOyej/GXD0goHHBuFb6QFmclzAhB8UIPtI6eSUYlpxwvWLe', '5ajzffP0RDAo5srXarXyvuyCu5zLCD4Ko5vh5vaycQ0SVE1bRG.jpg', '2022-03-12 06:54:19', '2022-03-12 06:54:19');
/*!40000 ALTER TABLE `employees` ENABLE KEYS */;

-- Dumping structure for table actone.inventories
CREATE TABLE IF NOT EXISTS `inventories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `pname` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stocks` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table actone.inventories: ~0 rows (approximately)
/*!40000 ALTER TABLE `inventories` DISABLE KEYS */;
INSERT INTO `inventories` (`id`, `pname`, `quantity`, `stocks`, `created_at`, `updated_at`) VALUES
	(1, 'Hatdog', '10', '50', '2022-03-11 23:00:54', '2022-03-11 23:00:54');
/*!40000 ALTER TABLE `inventories` ENABLE KEYS */;

-- Dumping structure for table actone.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table actone.migrations: ~2 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2019_08_19_000000_create_failed_jobs_table', 1),
	(3, '2022_01_20_071635_create_branches_table', 1),
	(4, '2022_01_20_212131_create_employees_table', 2),
	(5, '2022_01_21_142113_create_customers_table', 3),
	(6, '2022_01_30_094649_create_branch__employees_table', 4),
	(7, '2022_03_05_084427_create_inventories_table', 5),
	(8, '2022_03_11_071318_create_products_table', 6),
	(9, '2022_03_11_084110_create_sales_table', 7);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table actone.products
CREATE TABLE IF NOT EXISTS `products` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_arrival` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `expiry` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sprice` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `oprice` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table actone.products: ~2 rows (approximately)
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` (`id`, `name`, `code`, `date_arrival`, `expiry`, `sprice`, `oprice`, `created_at`, `updated_at`) VALUES
	(1, 'Hatdog', 'HAT', 'March 10, 2022', 'March 30, 2022', '100', '80', '2022-03-11 23:01:18', '2022-03-11 23:01:18'),
	(2, 'Product 1', 'PROD', 'March 10, 2022', 'April 10, 2022', '90', '80', '2022-03-12 08:08:27', '2022-03-12 08:08:27');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;

-- Dumping structure for table actone.sales
CREATE TABLE IF NOT EXISTS `sales` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table actone.sales: ~2 rows (approximately)
/*!40000 ALTER TABLE `sales` DISABLE KEYS */;
INSERT INTO `sales` (`id`, `name`, `quantity`, `price`, `total`, `created_at`, `updated_at`) VALUES
	(49, 'Hatdog', '2', '100', '200', '2022-03-12 10:18:45', '2022-03-12 10:18:45'),
	(50, 'Product 1', '5', '90', '450', '2022-03-12 10:18:53', '2022-03-12 10:18:53');
/*!40000 ALTER TABLE `sales` ENABLE KEYS */;

-- Dumping structure for table actone.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `utype` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table actone.users: ~2 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `utype`, `password`, `photo`, `created_at`, `updated_at`) VALUES
	(25, 'Kiki', 'Agent', '$2y$10$BhkIcqC23OjWOFcfUrKKneIh9oWPt03fUIXLyWBGmBwNDR6cDKW6K', NULL, '2022-03-04 09:10:10', '2022-03-04 09:10:10'),
	(33, 'Employee_One', 'Agent', '$2y$10$dvB1myoGIvtanshptSWrruOoNpu1Ddimm.fOpV33mUmDK0l3TV/S2', '9efNkN91oFfsE2WsZ7yHTXzpW4KmrpTkDJdaZzONEXBodo5NSE.jpg', '2022-03-11 09:47:12', '2022-03-11 09:47:12'),
	(34, 'Employee_Two', 'Staff', '$2y$10$iyhvIu03iwycroCFP.2/b.sAPCmrLGf7ZLbbeO2Zfl9Pked.ya2iW', '5ajzffP0RDAo5srXarXyvuyCu5zLCD4Ko5vh5vaycQ0SVE1bRG.jpg', '2022-03-12 06:54:19', '2022-03-12 06:54:19');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
