<?php

use Illuminate\Support\Facades\Route;

Route::get('login', 'LoginController@index')->name('login');
Route::post('login-new', 'LoginController@store')->name('pasok');
Route::get('logout', 'LoginController@logout')->name('gawas');

Route::get('/branches', 'BranchController@index')->name('branch.index');
Route::post('/branches/store', 'BranchController@store');
Route::get('/branches/edit/{id}', 'BranchController@edit')->name('branch.edit');
Route::get('/branches/delete/{id}', 'BranchController@delete')->name('branch.delete');
Route::patch('/branches/update/{id}', 'BranchController@update');

Route::get('/employees', 'EmployeeController@index')->name('employee.index');
Route::post('/employees/store', 'EmployeeController@store');
Route::get('/employees/show', 'EmployeeController@show')->name('employee.show');
Route::get('/employees/edit/{id}', 'EmployeeController@edit')->name('employee.edit');
Route::get('/employees/delete/{id}', 'EmployeeController@delete')->name('employee.delete');
Route::patch('/employees/update/{id}', 'EmployeeController@update');
Route::get('/employees/bu_delete/{id}', 'EmployeeController@delete_assigned')->name('bu.delete');

Route::get('/customers', 'CustomerController@index')->name('customer.index');
Route::post('/customers/store', 'CustomerController@store');
Route::get('/customers/edit/{id}', 'CustomerController@edit')->name('customer.edit');
Route::get('/customers/delete/{id}', 'CustomerController@delete')->name('customer.delete');
Route::patch('/customers/update/{id}', 'CustomerController@update');

Route::get('/inventories', 'InventoryController@index')->name('inventory.index');
Route::post('/inventories/store', 'InventoryController@store');
Route::get('/inventories/edit/{id}', 'InventoryController@edit')->name('inventory.edit');
Route::get('/inventories/delete/{id}', 'InventoryController@delete')->name('inventory.delete');
Route::patch('/inventories/update/{id}', 'InventoryController@update');

Route::get('/products', 'ProductController@index')->name('product.index');
Route::post('/products/store', 'ProductController@store');
Route::get('/products/edit/{id}', 'ProductController@edit')->name('product.edit');
Route::get('/products/delete/{id}', 'ProductController@delete')->name('product.delete');
Route::patch('/products/update/{id}', 'ProductController@update');

Route::get('/sales', 'SaleController@index')->name('sale.index');
Route::post('/sales/store', 'SaleController@store');
Route::get('/sales/delete/{id}', 'SaleController@delete')->name('sale.delete');

Route::get('/products/export', 'ProductsExportController@export')->name('product.export');
Route::get('/sales/export', 'SalesExportController@export')->name('sale.export');

Route::get('/reports', 'ReportController@index')->name('report.index');
